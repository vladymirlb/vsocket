#include <iostream>
#include "VSocket.h"

int main(int argc, char **argv) {
   std::cout << "Hello, world!" << std::endl;
   struct sockaddr_in addr1, addr2;
   addr1.sin_family = AF_INET;
   addr1.sin_port = 10;
   
   std::cout << addr1.sin_port << std::endl;
   std::cout << "SAD!\n";
   addr2 = addr1;
   
   std::cout << addr2.sin_port << std::endl;
   return 0;
}
