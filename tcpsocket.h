#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include "VSocket.h"


class TCPSocket : public VSocket
{

public:
    virtual void disconnect();
    virtual void connect(const char* address, int port);
    TCPSocket();
    virtual ~TCPSocket();
};

#endif // TCPSOCKET_H
